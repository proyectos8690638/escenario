Shader"Custom/TextureOverSourgace"
{
    Properties
    {
        _MainText ("MainTexture", 2D) = "White" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _OverText ("OverTexture", 2D) = "White" {}
        _Amount ("Amount", Range(0, -3)) = 0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        CGPROGRAM
        
        #pragma surface surf Standard fullforwardshadows

        sampler2D _MainText;
        half4 _Color;
        sampler2D _OverText;
        fixed _Amount;
        
        struct Input
        {
            half2 uv_MainText;
            half2 uv_OverText;
    
        };
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            half4 MainText = tex2D(_MainText, IN.uv_MainText);
            half4 OverText = tex2D(_OverText, IN.uv_OverText);

            if (dot(OverText - 1, MainText) >= _Amount)
            {
                o.Albedo = OverText.rgb * _Color.rgb;
            }
            else
            {
                o.Albedo = MainText.rgb;
            }

        }
        ENDCG
    }

}
