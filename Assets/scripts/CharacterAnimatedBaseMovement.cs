using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]


public class CharacterAnimatedBaseMovement : MonoBehaviour
{
    public AddForceMine ADF;

    public float rotationSpeed = 4f;

    public float rotationThreshold = 0.3f;

    public int degreesToTurn = 160;

    public float fuerzadesalto = 8f; 


    [Header("Animator Parameters")]

    public string motionParam = "motion";
    public string mirrorIdleParam = "mirorIdle";
    public bool animateInAction;


    [Header("Animation Smoothing")]
    [Range(0, 1f)]

    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    public float Speed;

    private Vector3 desiredMoveDirection;
    private CharacterController charactercontroller;
    private Animator animator;

    private bool mirrorIdle;
    public bool puedosaltar;
    private bool jumps;
    public float contador;
    public int animacion_actual;


    // Start is called before the first frame update
    void Start()
    {
        charactercontroller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {

        //Calculate Input Magnitude
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;
        if(vInput < 0f)
        {
            Speed = -1;
        }

        if (Speed >= Speed - rotationThreshold && dash)
        {
            if (vInput > 0f) Speed = 2f;
            else Speed = -2f;
        }
   

        if (Speed > rotationThreshold)
        {

            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;                
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                                Quaternion.LookRotation(desiredMoveDirection),
                                                rotationSpeed * Time.deltaTime);


        }

        else if (Speed < rotationThreshold)
        {

            animator.SetBool(mirrorIdleParam, mirrorIdle);
            //Stop the character
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }
        if (jump)
        {
            jumps = true;
        }
        if (!jump)
        {
            jumps = false;
        }
        if (puedosaltar && jumps)
        {
            ADF.AddImpact(new Vector3(-desiredMoveDirection.x, -1, -desiredMoveDirection.z), -fuerzadesalto);
            animator.SetBool("salto", true);
            puedosaltar = false;
        }
        if (puedosaltar && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f && animator.GetCurrentAnimatorStateInfo(0).IsName("salto"))
        {
            animator.SetBool("salto", false);
        }
    }
    private void Update()
    {
        if (Speed == 0)
        {
            contador += Time.deltaTime;
            if (animateInAction == false)
            {   
                
                if(contador >= 12)
                {
                    if(animacion_actual == 1)
                    {
                        animator.SetFloat("Idle_type", 0);
                    }
                    if (animacion_actual == 0)
                    {
                        animator.SetFloat("Idle_type", 1);
                    }
                    if(animacion_actual == 1)
                    {
                        animacion_actual = 0;
                    }
                    else 
                    {
                        animacion_actual = 1;
                    }
                    contador = 0;
                }
                animateInAction = true;
            }
            if (animateInAction == true)
            {

                animateInAction = false;

            }
        }
        if(Speed != 0)
        {
            contador = 0;
        }
    }



    private void OnAnimatorIK(int layerIndex)
    {

        if (Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if (distanceToRightFoot > distanceToLeftFoot)
        {

            mirrorIdle = true;

        }
        else
        {

            mirrorIdle = false;
        }

    }
}
